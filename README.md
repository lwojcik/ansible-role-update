# ansible-role-update

Ansible role for manual updates of Debian-based system. Tested with Debian 10 and Raspberry Pi OS Buster.

## License

Licensed under MIT License. See [LICENSE](https://github.com/lwojcik/ansible-role-update/blob/master/LICENSE) for more information.